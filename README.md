# mkdtimg

Sources for mkdtimg and its dependencies:
* <https://android.googlesource.com/platform/system/libufdt/+/refs/heads/master/utils/src>
* <https://android.googlesource.com/platform/system/libufdt/+/refs/heads/master/include>
* <https://android.googlesource.com/platform/external/dtc/+/refs/heads/master/libfdt>
* <https://android.googlesource.com/platform/system/libufdt/+/refs/heads/master/sysdeps>

To compile it, clone it first and then run `./compile.sh`. The binary will be in `bin/` after it's done. 
